package de.walamana.reloadedgen;

import de.walamana.reloadedgen.config.Configuration;
import de.walamana.reloadedgen.mysql.MySQLConnection;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ReloadedGen extends JavaPlugin implements Listener {


    public static final String TAG = "[ReloadedGen] ";
    public static Configuration config;
    public MySQLConnection con;
    public ChatFilter filter;


    @Override
    public void onEnable() {

        config = new Configuration(this, "config.yml");
        config.loadConfig();

        con = new MySQLConnection(this);
        con.connect();
        filter = new ChatFilter(con, this);

        registerListener();
        registerCommands();


        System.out.println(TAG + "Plugin \"" + this.getName() + "\" loaded!");

        super.onEnable();
    }

    public void registerListener(){
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    public void registerCommands(){
        this.getCommand("chat").setExecutor(((commandSender, command, s, args) -> {

            if(commandSender instanceof Player){
                if(!commandSender.isOp()){
                    commandSender.sendMessage("§4You are not allowed to execute this command!");
                    return true;
                }
            }

            if(args.length >= 2){
                if(args[0].equalsIgnoreCase("add")){
                    con.update("INSERT IGNORE INTO blacklist(`keyword`) VALUES (\"" + args[1] +"\")");

                    commandSender.sendMessage("§3Word has been added to the blacklist!");
                    return true;
                }

                if(args[0].equalsIgnoreCase("remove")){
                    con.update("DELETE FROM `blacklist` WHERE `keyword` = \"" + args[1] + "\"");

                    commandSender.sendMessage("§3Word has been removed from the blacklist!");
                    return true;
                }
            }

            return true;
        }));
    }



    @EventHandler
    public void onAsyncChat(AsyncPlayerChatEvent e){

        e.setCancelled(true);

        final String player = e.getPlayer().getDisplayName();
        final String message = e.getMessage();

        filter.checkValid(message).then((invalids) -> {
            StringBuilder builder = new StringBuilder();

            builder.append("<");
            builder.append(player);
            builder.append(">");

            for(String s : message.split(" ")){
                if(invalids.contains(s)){
                    builder.append(" ****");
                }else{
                    builder.append(" ");
                    builder.append(s);
                }
            }

            String msg = filter.replaceUnicode(builder.toString());

            Bukkit.getServer().broadcastMessage(msg);
        });


    }


}
