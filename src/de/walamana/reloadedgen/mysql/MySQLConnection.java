package de.walamana.reloadedgen.mysql;

import de.walamana.reloadedgen.ReloadedGen;
import de.walamana.reloadedgen.util.Promise;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLConnection {

    public static final String TAG = "[ReloadedGen][MYSQL] ";

    private String host;
    private String port;
    private String user;
    private String password;
    private String database;

    private Connection con;

    private Plugin pl;

    public MySQLConnection(Plugin pl) {
        this.pl = pl;
    }

    public void connect(){

        host = ReloadedGen.config.getConfig().getString("host");
        port = ReloadedGen.config.getConfig().getString("port");
        user = ReloadedGen.config.getConfig().getString("user");
        password = ReloadedGen.config.getConfig().getString("password");
        database = ReloadedGen.config.getConfig().getString("database");

        try {
            con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(TAG + "Could not connect to database!");
        }
    }

    public Promise<ResultSet> getResults(String query){
        return new Promise<ResultSet>(pl){
            @Override
            public void run() {

                try {
                    ResultSet result = con.createStatement().executeQuery(query);
                    resolve(result);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        };
    }

    public void update(String query){

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    con.createStatement().execute(query);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(pl);

    }
}
