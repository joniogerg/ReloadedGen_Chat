package de.walamana.reloadedgen;

import de.walamana.reloadedgen.mysql.MySQLConnection;
import de.walamana.reloadedgen.util.Promise;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.plugin.Plugin;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ChatFilter {

    private MySQLConnection con;
    private Plugin pl;

    public ChatFilter(MySQLConnection con, Plugin pl) {
        this.con = con;
        this.pl = pl;
    }

    public Promise<List<String>> checkValid(String msg){

        String[] segments = msg.split(" ");

        final AtomicInteger counter = new AtomicInteger(segments.length);

        return new Promise<List<String>>(pl){
            @Override
            public void run() {
                List<String> invalid = new ArrayList<>();
                for(final String s : segments){
                    con.getResults("SELECT *, COUNT(*) as count FROM `blacklist` WHERE  LOCATE(`keyword`, '" + s.toLowerCase() + "')").then(resultSet -> {
                        try {

                            while(resultSet.next()){

                                int count = resultSet.getInt("count");
                                if(count >= 1){
                                    invalid.add(s);
                                }


                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        if(counter.decrementAndGet() == 0){
                            resolve(invalid);
                        }
                    });
                }
            }
        };

    }


    public String replaceUnicode(String string){
        int index = 0;
        String newString = string;
        while(index < string.length()){

            if(string.charAt(index) == '\\'){
                String unicode = string.substring(index + 2, index + 6);

                newString = newString.replace("\\u" + unicode, StringEscapeUtils.unescapeJava( "\\u"+ unicode) + "");
                System.out.println(newString);
                index += 6;
            }
            index++;
        }

        return newString;
    }

    public String filterIPs(String string){
        int index = 0;
        String newString = string;
        while(index < string.length()){

            char c = string.charAt(index);
            if(c >= '0' && c <= '9'){
                if(string.charAt(index + 1) >= '0' && string.charAt(index + 1) <= '9'){
                    
                }
            }
            index++;
        }

        return string;
    }
}
