package de.walamana.reloadedgen.util;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.function.Consumer;

public class Promise<T> implements Runnable{

    private Plugin pl;
    private Consumer<T> finished;

    public Promise(Plugin pl) {
        this.pl = pl;
        new BukkitRunnable() {
            @Override
            public void run() {
                Promise.this.run();
            }
        }.runTaskAsynchronously(pl);
    }


    public void then(Consumer<T> finished){
        this.finished = finished;
    }

    protected void resolve(T value){


        if(finished != null) {
            new BukkitRunnable() {
                @Override
                public void run() {
                        finished.accept(value);
                }
            }.runTask(pl);
        }
    }

    @Override
    public void run() {

    }
}
