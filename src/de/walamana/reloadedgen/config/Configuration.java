package de.walamana.reloadedgen.config;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class Configuration {

    public static final String TAG = "[ReloadedGen][CONFIG] ";

    private File file;
    private FileConfiguration fileConfiguration;


    public Configuration(Plugin pl, String fileName){
        file = new File(pl.getDataFolder() + "/" + fileName);

        if(!pl.getDataFolder().exists()){
            pl.getDataFolder().mkdirs();
        }

        if(!file.exists()){
            try {
                file.createNewFile();
                saveDefaults();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void loadConfig(){

        fileConfiguration = new YamlConfiguration();


        try {
            fileConfiguration.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

    }

    public void saveConfig(){

        if(fileConfiguration == null)
            return;

        try {
            fileConfiguration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveDefaults(){

        if(fileConfiguration == null){
            fileConfiguration = new YamlConfiguration();
        }

        fileConfiguration.addDefault("host", "localhost");
        fileConfiguration.addDefault("port", "3306");
        fileConfiguration.addDefault("user", "");
        fileConfiguration.addDefault("password", "");
        fileConfiguration.addDefault("database", "");

        fileConfiguration.options().copyDefaults(true);
        try {
            fileConfiguration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public FileConfiguration getConfig(){
        if(fileConfiguration == null){
            loadConfig();
        }
        return fileConfiguration;
    }

}
